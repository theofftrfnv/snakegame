// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ASnakeElementBase;
class AActor;
#ifdef SNAKEGAME_Snake_generated_h
#error "Snake.generated.h already included, missing '#pragma once' in Snake.h"
#endif
#define SNAKEGAME_Snake_generated_h

#define SnakeGame_Source_SnakeGame_Snake_h_24_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSnakeElementOvelap) \
	{ \
		P_GET_OBJECT(ASnakeElementBase,Z_Param_OverlappedElement); \
		P_GET_OBJECT(AActor,Z_Param_Other); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SnakeElementOvelap(Z_Param_OverlappedElement,Z_Param_Other); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_Snake_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSnakeElementOvelap) \
	{ \
		P_GET_OBJECT(ASnakeElementBase,Z_Param_OverlappedElement); \
		P_GET_OBJECT(AActor,Z_Param_Other); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SnakeElementOvelap(Z_Param_OverlappedElement,Z_Param_Other); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_Snake_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnake(); \
	friend struct Z_Construct_UClass_ASnake_Statics; \
public: \
	DECLARE_CLASS(ASnake, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnake)


#define SnakeGame_Source_SnakeGame_Snake_h_24_INCLASS \
private: \
	static void StaticRegisterNativesASnake(); \
	friend struct Z_Construct_UClass_ASnake_Statics; \
public: \
	DECLARE_CLASS(ASnake, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnake)


#define SnakeGame_Source_SnakeGame_Snake_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnake(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnake) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake(ASnake&&); \
	NO_API ASnake(const ASnake&); \
public:


#define SnakeGame_Source_SnakeGame_Snake_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnake(ASnake&&); \
	NO_API ASnake(const ASnake&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnake); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnake); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnake)


#define SnakeGame_Source_SnakeGame_Snake_h_24_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_Snake_h_21_PROLOG
#define SnakeGame_Source_SnakeGame_Snake_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_Snake_h_24_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_Snake_h_24_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_Snake_h_24_INCLASS \
	SnakeGame_Source_SnakeGame_Snake_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_Snake_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_Snake_h_24_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_Snake_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_Snake_h_24_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_Snake_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_Snake_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::LEFT) \
	op(EMovementDirection::RIGHT) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
